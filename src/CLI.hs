{-# LANGUAGE BangPatterns #-}

module CLI
  ( ProgramOpts
  , programOpts
  , findRouteWithOpts
  , runCLI
  ) where

import Options.Applicative
import Data.Semigroup ((<>))
import System.Random
import System.IO
import System.IO.Error
import Data.Maybe
import Control.Concurrent
import Control.Concurrent.Async

import qualified  Genetic as G
import qualified  Cities  as C
import            Simulation
import            ProgressPrinter

data ProgramOpts = ProgramOpts
  { crossoverProb :: Float    -- ^ Part of population for crossover.
  , mutationProb  :: Float    -- ^ Part of population for mutation.
  , targetSize    :: Int      -- ^ Target selected population size.
  , nGenerations  :: Int      -- ^ Generations number to be produced.
  , nCities       :: Int      -- ^ Number of cities to generate.
  , minDist       :: Float    -- ^ Minimum allowed distance.
  , maxDist       :: Float    -- ^ Maximum allowed distance.
  , nSellers      :: Int      -- ^ Number of sellers.
  , resultsFile   :: FilePath -- ^ File name of the output results file.
  , citiesSeed    :: Int      -- ^ Random number generator seed for cities.
  , nSimulations  :: Int      -- ^ Number of simulations to be performed.
  , mapFile       :: FilePath -- ^ Path to the file with cities map.
  }

-- | CLI parser for program options.
programOpts :: Parser ProgramOpts
programOpts = ProgramOpts
  <$> option auto
    ( long    "crossover"
    <>short   'c'
    <>value   0.8
    <>help    "Part of population to be crossoered, 1 means 100%.")
  <*> option auto
    ( long    "mutation"
    <>short   'm'
    <>value   1.0
    <>help    "Part of population to be mutated, 1 means 100%.")
  <*> option auto
    ( long    "population-size"
    <>short   'p'
    <>value   50
    <>help    "Size of population.")
  <*> option auto
    ( long    "generations"
    <>short   'n'
    <>value   200
    <>help    "Number of generations to be used in evolution.")
  <*> option auto
    ( long    "cities"
    <>value   200
    <>help    "Number of cities to generate.")
  <*> option auto
    ( long    "min-distance"
    <>value   20
    <>help    "Minimum distance between cities.")
  <*> option auto
    ( long    "max-distance"
    <>value   400
    <>help    "Maximum distance between cities.")
  <*> option auto
    ( long    "sellers"
    <>value   2
    <>help    "Number of sellers i.e. number of routs to find.")
  <*> strOption
    ( long    "filename"
    <>short   'o'
    <>value   "results.txt"
    <>help    "Output file name for results.")
  <*> option auto
    ( long    "map-seed"
    <>value   0
    <>help    "Seed for generation cities map. Use same seed between program \
    \ runs for achieve reproducable city map for many simulations.")
  <*> option auto
    ( long    "simulations"
    <>short   's'
    <>value   1
    <>help    "Number of simulations performed for city map.")
  <*> strOption
    ( long    "map-file"
    <>value   ""
    <>help    "Use given file with map instead of generation. \
    \ Format is i x y per row, where i is id of city.")

performSimulations :: ProgramOpts -> IO ()
performSimulations opts = do
  seedMapIO <- randomIO
  let seedMap = if (citiesSeed opts) == 0 then seedMapIO else (citiesSeed opts)

  -- Create city map
  let cityMapGen
        = C.randomCityMap
          seedMap
          (nCities opts)
          ((minDist opts), (maxDist opts))
  cityMapFile <- mapFromFile (mapFile opts)
  let cityMap' = if (mapFile opts) == "" then cityMapGen else cityMapFile
  let cityMap = C.addDummyCities cityMap' (nSellers opts - 1)

  -- Write city map to output file
  outputFile <- openFile (resultsFile opts) WriteMode
  hPutStrLn outputFile $ "City map seed: " ++ show seedMap
  hPutStrLn outputFile $ "City map:"

  -- Create pretty progress printer configuration
  printer <- progressPrinter (nSimulations opts) 50
  listenForKill printer

  -- Perform simulations
  results <- mapConcurrently
              (findRouteWithOpts printer opts cityMap)
              [1..(nSimulations opts)]
  let stats = simulationsStats results

  disablePrinter printer

  -- Write results and plot routes
  C.drawRoute "Best" cityMap (bestRoute $ bestSimulation results)
  C.drawRoute "Worst" cityMap (bestRoute $ worstSimulation results)

  mapM_ ((hPutStrLn outputFile) . show) results
  hPutStrLn outputFile $ show stats
  print "\n"
  print stats

  hClose outputFile

  -- Wait for gnuplot start before exit
  threadDelay 1000000

-- | Perform simulation with given id, i.e. find route for given map.
findRouteWithOpts
  :: ProgressPrinter        -- ^ Progress printer configuration.
  -> ProgramOpts            -- ^ Program options.
  -> C.CityMap              -- ^ City map.
  -> Int                    -- ^ Simulation ID.
  -> IO (SimulationResult)  -- ^ Output simulation result.
findRouteWithOpts printer opts cityMap i = do
  seedIO      <- randomIO
  routesSeed  <- randomIO

  -- Prepare initial population of routes
  let nRouts          = targetSize opts
  let seeds           = take nRouts $ randoms $ mkStdGen routesSeed
  let initRoutes      = (C.randomRoute cityMap) <$> seeds
  let initPopulation  = G.makePopulation $ G.makeChromosome <$> initRoutes
  let initBestRoutes  = G.select (C.fitRoute cityMap) 1 initPopulation
  let initBestRoute   = head $ G.fromPopulation initBestRoutes

  -- Find best route by evolution
  let params
        = G.EvolutionParams
          (crossoverProb opts)
          (mutationProb opts)
          nRouts
          G.pmxCrossover
          G.reverseMutation
          seedIO
          (C.fitRoute cityMap)
          (nGenerations opts)
  newPopulation <- G.evolutionIO printer i params initPopulation
  let newBestRoutes = G.select (C.fitRoute cityMap) 1 newPopulation
  let !newBestRoute = head $ G.fromPopulation newBestRoutes

  return $ simulationResult cityMap newBestRoute i

badMapFile :: IOError -> IO String
badMapFile _ = return ""

mapFromFile :: FilePath -> IO C.CityMap
mapFromFile path = (C.readMapCsv . lines)
               <$> (readFile path) `catchIOError` badMapFile

runCLI :: IO ()
runCLI = performSimulations =<< execParser opts
  where
    opts = info (programOpts <**> helper)
      ( fullDesc
     <> progDesc "Uladzislau Harbuz, 2021, WSISIZ"
     <> header "Find optimal route for n sellers by genetic algorithm.")
