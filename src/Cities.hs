module Cities
  ( City
  , CityId
  , Point
  , Route
  , CityMap
  , Distance
  , distance
  , nCities
  , readMapCsv
  , makeSimmetricMap
  , randomRoute
  , routeDistances
  , routeCost
  , routeCostBalanced
  , randomCityMap
  , routeToCities
  , drawRoute
  , addDummyCities
  , infinity
  , splitRoute
  , fitRoute
  ) where

import Data.Matrix
import Data.List
import Data.List.Split as LS
import Data.Ord
import Data.Maybe
import System.Random as R
import System.Random.Shuffle
import Graphics.Gnuplot.Simple

import Genetic

-- | Point on the map with x and y.
type Point = (Float,Float)

-- | Unique city identifier.
type CityId = Int

-- | City is represented by id in city-cost matrix.
data City = City
  { point :: Point  -- ^ City position on the map.
  , id    :: CityId -- ^ City id.
  } deriving (Show)

-- | Route is just a list of cities in order of visiting.
type Route = [CityId]

-- | Distance between cities.
type Distance = Float

-- | City map contains cities as indices of rows and columns and distance
-- between it at matrix cell.
data CityMap = CityMap
  { distances :: Matrix Distance  -- ^ Distances between all cities.
  , cities    :: [City]           -- ^ List of all cities.
  , splitters :: [CityId]         -- ^ Cities which split route for n sellers.
  }

instance Show CityMap where
  show m = show $ distances m

instance Eq City where
  (==) (City _ a) (City _ b) = a == b

instance Ord City where
  compare (City _ a) (City _ b) = compare a b

infinity = 1/0

-- | Read city from string in csv format separate by spaces, where format is:
-- <city id> <x> <y>
readCityCsv :: String -> City
readCityCsv line = City (x, y) i
  where
    [si, sx, sy] = words line
    i = read si :: CityId
    x = read sx :: Float
    y = read sy :: Float

-- | Read cities map from list of CSV lines with space separator.
readMapCsv :: [String] -> CityMap
readMapCsv xs = makeSimmetricMap (readCityCsv <$> xs) []

-- | Get distance between two cities if both are presented at given map.
distance :: CityMap -> CityId -> CityId -> Maybe Distance
distance m a b = safeGet a b (distances m)

-- | Get number of cities in map.
nCities :: CityMap -> Int
nCities = nrows . distances

-- | Get distance between two cities got from it points.
-- This function is less efficient than getting distance from CityMap,
-- so use it only for map initialization.
distance' :: City -> City -> Distance
distance' (City (x1,y1) _) (City (x2,y2) _)
  | x1 == x2 && y1 == y2  = infinity
  | otherwise             = sqrt $ (dist2 x2 x1) + (dist2 y2 y1)
  where
    dist2 a b = (a - b) ^^ 2

-- | Make simmetric city map from given cities list. This
-- function creates mirrored matrix of distances
-- with Infinity diagonale from given matrix.
makeSimmetricMap :: [City] -> [CityId] -> CityMap
makeSimmetricMap cities = CityMap (matrix n n mkDistance) cities
  where
    mkDistance :: (CityId, CityId) -> Distance
    mkDistance (r, c)
      | c == r    = infinity
      | otherwise = distance' (cities !! (r - 1)) (cities !! (c - 1))
    n = length cities

-- | Get random route from given seed.
randomRoute :: CityMap -> Int -> Route
randomRoute m seed = shuffle' [1..n] n $ mkStdGen seed
  where
    n = nCities m

-- | Transform route from cities list to distances between theese cities list.
routeDistances :: CityMap -> Route -> Maybe [Distance]
routeDistances m r = sequence $ zipWith (distance m) r (last r:r)

-- | Split route for subroutes by given list of splitter cities.
splitRoute :: CityMap -> Route -> [Route]
splitRoute m r = mergeLast $ LS.split (keepDelimsR $ whenElt isSplitter) r
  where
    mergeLast xs = (init.init $ xs) ++ [(last.init $ xs) ++ last xs]
    isSplitter x = x `elem` (splitters m)

-- | Get total route cost. If some of cities in route are out of map,
-- return Nothing.
routeCost :: CityMap -> Route -> Maybe Distance
routeCost m r = sumSubroutes $ sequence $ routeDistances m <$> subroutes
  where
    subroutes       = splitRoute m r
    sumSubroutes x  = fmap sum $ fmap (map sum) x

-- | Get maximum cost of route costs of all sellers for balancing workload.
routeCostBalanced :: CityMap -> Route -> Maybe Distance
routeCostBalanced m r = maxCost $ sequence $  routeDistances m <$> subroutes
  where
    subroutes = splitRoute m r
    maxCost x = fmap maximum $ fmap (map sum) x

-- | Generate random city map of given size.
randomCityMap
  :: Int            -- ^ Random number generator seed.
  -> Int            -- ^ Number of cities to generate.
  -> (Float, Float) -- ^ Range of axis of generated cities.
  -> CityMap        -- ^ Output generated map.
randomCityMap seed n bnds = makeSimmetricMap cities []
  where
    cities      = zipWith City points [1..]
    points      = zip (randList g1) (randList g2)
    randList g  = take n $ randomRs bnds g
    (g1, g2)    = R.split $ mkStdGen seed

-- | Make list of cities from route. Return nothing if some cities in route
-- are not present on the map.
routeToCities :: CityMap -> Route -> Maybe [City]
routeToCities m r = sequence $ idToCity <$> r
  where
    idToCity cid
      | cid > csLen || cid <= 0   = Nothing
      | otherwise                 = Just $ cs !! (cid - 1)
    cs = sort $ cities m
    csLen = length cs

-- | Draw route on the plot.
drawRoute :: String -> CityMap -> Route -> IO ()
drawRoute title m r = plotListsStyle [Title title] charts
  where
    points x = fromMaybe [] $ (map point) <$> loopRoute <$> routeToCities m x
    loopRoute xs = xs ++ [head xs]
    charts = zip styles $ [points r] ++ routesPoints
    styles = [defaultStyle {plotType = Points}] ++ repeat defaultStyle
    routes = splitRoute m r
    routesPoints = points <$> splitRoute m r

-- | Add n dummy cities to the map (it is copy of the first city) for resolve
-- TSP prolem for n sellers.
addDummyCities :: CityMap -> Int -> CityMap
addDummyCities m n = makeSimmetricMap (cs ++ dummies) (1 : splitters)
  where
    cs = (cities m)
    dummies = zipWith replaceId splitters $ repeat $ head cs
    replaceId i city = city {Cities.id = i}
    splitters = take n $ [length cs + 1 ..]

-- | Fitness function for MTSP problem with cities.
fitRoute :: CityMap -> Chromosome CityId -> Distance
fitRoute m x = fromMaybe infinity $ routeCostBalanced m $ fromChromosome x
