module ProgressPrinter
  ( ProgressPrinter
  , ProgressBar
  , progressPrinter
  , progressBar
  , printProgress
  , listenForKill
  , disablePrinter
  ) where

import Control.Concurrent.MVar
import Control.Concurrent
import System.IO
import Control.Monad
import System.Exit
import System.Posix.Signals

-- | Configuration of the progress printer.
data ProgressPrinter = ProgressPrinter
  { cliLock   :: MVar ()  -- ^ Lock variable for console.
  , nThreads  :: Int      -- ^ Total number of threads to be drawn.
  , width     :: Int      -- ^ Progress bars width in characters.
  }

-- | One progress bar description.
data ProgressBar a = ProgressBar
  { threadId  :: Int  -- ^ Thread D of the progress bar, i.e. row id.
  , actual    :: a    -- ^ Actual value of the progress bar.
  , total     :: a    -- ^ Maximum value of the progress bar.
  }

-- | Create new progress printer instance.
progressPrinter :: Int -> Int -> IO ProgressPrinter
progressPrinter n w = do
  lock        <- newMVar ()
  return $ ProgressPrinter lock n w

-- | Make progress bar instance.
progressBar :: Int -> a -> a -> ProgressBar a
progressBar = ProgressBar

-- | Pretty print the given progress bar.
printProgress
  :: (Integral a, Show a)
  => ProgressPrinter      -- ^ Printer configuration.
  -> ProgressBar a        -- ^ Bar to be printed.
  -> IO ()
printProgress conf bar = do
  withMVar (cliLock conf) $ \_ -> do
    putStr $ "\r[" ++ progBar ++ spaces  ++ "]"
          ++ " Running " ++ show (actual bar)
          ++ " generation from " ++ show (total bar) ++ "..."
    hFlush stdout
  where
    progBar   = replicate barLen '#'
    spaces    = replicate ((width conf) - barLen) ' '
    barLen    = floor $ step * (fromIntegral $ actual bar)
    step      = (fromIntegral $ width conf) / (fromIntegral $ total bar)

-- | Listen for signals got from CLI and close program if Ctrl+C pressed.
listenForKill :: ProgressPrinter -> IO ()
listenForKill printer = do
  _ <- installHandler softwareTermination (Catch closeProgram) Nothing
  return ()
  where
    closeProgram = do
      withMVar (cliLock printer) $ \_ -> disablePrinter printer
      exitSuccess

-- | Disable Vty printing and return to usual stdout mode.
disablePrinter :: ProgressPrinter -> IO ()
disablePrinter _ = return ()
