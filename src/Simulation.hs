module Simulation
  ( SimulationResult (..)
  , SimulationsStats (..)
  , simulationResult
  , simulationsStats
  , bestSimulation
  , worstSimulation
  ) where

import Data.Maybe
import System.Random
import Data.List

import qualified  Genetic as G
import qualified  Cities  as C

-- | Single evolution simulation results.
data SimulationResult = SimulationResult
  { bestRoute     :: C.Route        -- ^ The best found routes.
  , totalCost     :: C.Distance     -- ^ Total cost of the best route.
  , balancedCost  :: C.Distance     -- ^ The balanced cost of the best route.
  , costs         :: [C.Distance]   -- ^ Every subroute cost.
  , simulationId  :: Int            -- ^ Simulation identifier.
  , simCityMap    :: C.CityMap      -- ^ Simulation city map.
  }

-- | Statistics from multiple simulations performed on the same city map.
data SimulationsStats = SimulationsStats
  { avgTotalCost    :: C.Distance       -- ^ Average total cost.
  , avgBalancedCost :: C.Distance       -- ^ Average balanced cost.
  , bestSim         :: SimulationResult -- ^ The best simulation.
  , worstSim        :: SimulationResult -- ^ The worst simulation.
  }

instance Show SimulationResult where
  show res
    =  "Routes: \n"
    ++ show (C.splitRoute (simCityMap res) (bestRoute res)) ++ "\n"
    ++ "Total cost:      "  ++ show (totalCost res)         ++ "\n"
    ++ "Balanced cost:   "  ++ show (balancedCost res)      ++ "\n"
    ++ "Subroutes costs: "  ++ show (costs res)             ++ "\n"
    ++ "Simulation ID:   "  ++ show (simulationId res)      ++ "\n"

instance Eq SimulationResult where
  (==) s1 s2 = (balancedCost s1) == (balancedCost s2)

instance Ord SimulationResult where
  compare s1 s2 = (balancedCost s1) `compare` (balancedCost s2)

instance Show SimulationsStats where
  show stats
    =  "----------------------- Simulations statistics -------------------\n"
    ++ "Average total cost:    "  ++ show (avgTotalCost stats)    ++ "\n"
    ++ "Average balanced cost: "  ++ show (avgBalancedCost stats) ++ "\n\n"
    ++ "The best simulation:\n"   ++ show (bestSim stats)         ++ "\n\n"
    ++ "The worst simulation:\n"  ++ show (worstSim stats)        ++ "\n"
    ++ "------------------------------------------------------------------\n"

-- | Get the best simulation from list.
bestSimulation :: [SimulationResult] -> SimulationResult
bestSimulation = head . sort

-- | Get the worst simulation from list.
worstSimulation :: [SimulationResult] -> SimulationResult
worstSimulation = head . reverse . sort

-- | Make simulation result from given parameters.
simulationResult :: C.CityMap -> C.Route -> Int -> SimulationResult
simulationResult cityMap route i
  = SimulationResult route tCost bCost cs i cityMap
  where
    tCost = fromMaybe C.infinity $ C.routeCost cityMap route
    bCost = fromMaybe C.infinity $ C.routeCostBalanced cityMap route
    cs    =   fromMaybe C.infinity
          <$> C.routeCost cityMap
          <$> C.splitRoute cityMap route

-- | Get statistics from list of simulation results.
simulationsStats :: [SimulationResult] -> SimulationsStats
simulationsStats results = SimulationsStats avgTotal avgBalanced best worst
  where
    avgTotal      = (/n) $ sum $ totalCost <$> results
    avgBalanced   = (/n) $ sum $ balancedCost <$> results
    best          = bestSimulation results
    worst         = worstSimulation results
    n             = fromIntegral $ length results
