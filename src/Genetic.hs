{-# LANGUAGE DeriveGeneric #-}

module Genetic
  ( Chromosome
  , Population
  , Competitor
  , EvolutionParams (..)
  , makeChromosome
  , fromChromosome
  , fromPopulation
  , makePopulation
  , makeCompetitor
  , pmxCrossover
  , reverseMutation
  , select
  , crossPopulation
  , generation
  , evolution
  , evolutionIO
  ) where

import System.Random as R
import Data.List.Split
import Data.Ord
import Data.Eq
import Data.List
import Control.Monad
import Control.DeepSeq
import GHC.Generics

import ProgressPrinter

-- | Wrap sequence of genes into chromosome. User should use custom type
-- from its problem domain as gene.
data Chromosome geneT = Chromosome [geneT]
  deriving (Show, Generic)

-- | Wrap set of chromosomes into population
data Population geneT = Population [Chromosome geneT]
  deriving (Show, Generic)

-- | Chromosome becomes competitor for selection when fitness is known.
data Competitor geneT fitT = Competitor
  { chromosome  :: Chromosome geneT
  , fitness     :: fitT
  }

-- Internal type aliases for operators
type CrossoverOp geneT
  = (Int
  -> (Chromosome geneT, Chromosome geneT)
  -> (Chromosome geneT, Chromosome geneT)
  )
type MutationOp geneT = (Int -> Chromosome geneT -> Chromosome geneT)
type FitnessOp geneT fitT = (Chromosome geneT -> fitT)

data EvolutionParams geneT fitT = EvolutionParams
  { crossoverProb :: Float                -- ^ Crossover probability.
  , mutationProb  :: Float                -- ^ Mutation probability.
  , targetSize    :: Int                  -- ^ Target selected population size.
  , crossoverOp   :: CrossoverOp geneT    -- ^ Crossover operator
  , mutationOp    :: MutationOp geneT     -- ^ Mutation operator
  , seed          :: Int                  -- ^ Random number generator seed.
  , fitnessOp     :: FitnessOp geneT fitT -- ^ Fitness operation for selection.
  , nGenerations  :: Int                  -- ^ Generations number to be produced.
  }

instance Eq fitT => Eq (Competitor geneT fitT) where
  (==) (Competitor _ a) (Competitor _ b) = a == b

instance Ord fitT => Ord (Competitor geneT fitT) where
  compare (Competitor _ a) (Competitor _ b) = compare a b

instance NFData geneT => NFData (Chromosome geneT)
instance NFData geneT => NFData (Population geneT)

-- | Make chromosome from user type list
makeChromosome :: [geneT] -> Chromosome geneT
makeChromosome = Chromosome

-- | Extract list of genes from chromosome
fromChromosome :: Chromosome geneT -> [geneT]
fromChromosome (Chromosome x) = x

-- | Make population from list of chromosomes
makePopulation :: [Chromosome geneT] -> Population geneT
makePopulation = Population

-- | Make list of genes sequences from population.
fromPopulation :: Population geneT -> [[geneT]]
fromPopulation (Population cs) = fromChromosome <$> cs

-- | Make competitor from individual chromosome by matching it fitness by
-- given function.
makeCompetitor
  :: (Chromosome geneT -> fitT)
  -> Chromosome geneT
  -> Competitor geneT fitT
makeCompetitor fit genes = Competitor genes (fit genes)

-- | Select n best individuals from given population. The best individual is
-- one with the minimum fitness value.
select
  :: Ord fitT
  => FitnessOp geneT fitT -- ^ Fitness function for individual.
  -> Int                  -- ^ Size of target population of best individuals.
  -> Population geneT     -- ^ Source population.
  -> Population geneT     -- ^ Population of n best individuals from source.
select fitness n (Population individuals)
  = makePopulation $ chromosome <$> successors
  where
    successors  = take n $ sort competitors
    competitors = makeCompetitor fitness <$> individuals

-- | Split given list for n parts of random sizes and positions.
splitByRand :: Int -> Int -> [a] -> [[a]]
splitByRand seed n xs = splitPlacesBlanks idxs xs
  where
    idxs      = splitIdxs ++ [len - (sum splitIdxs)]
    splitIdxs = take (n - 1) $ randomRs (0, len) $ mkStdGen seed
    len       = length xs

-- | Crossover two given chromosomes with partially matched crossover
-- algorithm. Partition is made randomly by given seed.
pmxCrossover :: Eq geneT => CrossoverOp geneT
pmxCrossover seed (Chromosome a, Chromosome b) = cross (split a) (split b)
  where
    split = splitByRand seed 3
    cross a'@(a1:a2:as) b'@(b1:b2:bs)
      = (crossMap a' b2, crossMap b' a2)
      where
        crossMap (x1:x2:xs) ys = Chromosome $ concat (f x1 : ys : (f <$> xs))
          where
            f x = map (f' mapping) x
            f' [] res = res
            f' ((y, x):rest) res
              | res `elem` ys =
                if res == y
                  then f' mapping x
                  else f' rest res
              | otherwise = res
            mapping = zip ys x2

-- | Perform reverse mutation of the random part of given chromosome. Random
-- part of chromosome is choosen by given seed.
reverseMutation :: Int -> Chromosome geneT -> Chromosome geneT
reverseMutation seed (Chromosome x) = Chromosome $ concat $ mutate parts
  where
    parts = splitByRand seed 3 x
    mutate (x1:x2:xs) = (x1:reverse x2:xs)

-- | Crossover given population by crossing given percent of
-- random individuals.
crossPopulation
  :: Eq geneT
  => CrossoverOp geneT  -- ^ Crossover operator.
  -> Int                -- ^ Random number generator seed.
  -> Float              -- ^ Probability of crossout.
  -> Population geneT   -- ^ Source population.
  -> Population geneT   -- ^ Source population with its children.
crossPopulation cross seed prob (Population oldGen) = makePopulation $ newGen
  where
    newGen      = oldGen ++ (catPairs $ zipWith cross seeds parents)
    parents     = (\ (a,b) -> (oldGen !! a, oldGen !! b)) <$> randomPairs
    randomPairs = zip (randomList g1) (randomList g2)
    (g1, g2)    = R.split $ mkStdGen seed
    randomList g
      = take (floor $ (fromIntegral oldGenSize) * prob)
      $ randomRs (0, oldGenSize) g
    oldGenSize  = length oldGen - 1
    catPairs    = foldl (\ xs (a,b) -> xs ++ [a,b]) []
    seeds       = randoms $ mkStdGen seed

-- | Perform random mutation of given percent of population individuals.
mutatePopulation
  :: MutationOp geneT -- ^ Mutation operator.
  -> Int              -- ^ Random number generator seed.
  -> Float            -- ^ Probability of mutation.
  -> Population geneT -- ^ Source population.
  -> Population geneT -- ^ Source population with additional mutated individuals.
mutatePopulation mutate seed prob (Population oldGen) = makePopulation $ newGen
  where
    newGen      = oldGen ++ mutants
    mutants     = zipWith mutate seeds randomChromosomes
    randomChromosomes
      = take (floor $ (fromIntegral oldGenSize) * prob)
      $ (\ i -> oldGen !! i) <$> randomIdxs
    randomIdxs  = randomRs (0, oldGenSize) $ mkStdGen seed
    seeds       = randoms $ mkStdGen seed
    oldGenSize  = length oldGen - 1

-- | Make one individuals generation performing crossover, mutations
-- and selection according given configuration.
generation
  :: (Eq geneT, Ord fitT)
  => EvolutionParams geneT fitT -- ^ Generation evolution parameters.
  -> Population geneT           -- ^ Input parent population.
  -> Population geneT           -- ^ Next generation population.
generation config = selection . mutation . crossover
  where
    selection = select (fitnessOp config) (targetSize config)
    crossover = crossPopulation
      (crossoverOp config)
      (seed config)
      (crossoverProb config)
    mutation = mutatePopulation
      (mutationOp config)
      (seed config)
      (mutationProb config)

-- | Perform evolution using given configuration.
evolution
  :: (Eq geneT, Ord fitT)
  => EvolutionParams geneT fitT -- ^ Evolution parameters.
  -> Population geneT           -- ^ Initial population.
  -> Population geneT           -- ^ Finish population.
evolution config initPopulation
  = foldl nextGen initPopulation [1..(nGenerations config)]
  where
    nextGen currentGen i = generation (replaceSeed config i) currentGen
    seeds = take (nGenerations config) $ randoms $ mkStdGen (seed config)
    replaceSeed c i = c {seed = seeds !! (i - 1)}

-- | Same as evolution, but log write progress information into stdout.
evolutionIO
  :: (Eq geneT, NFData geneT, Ord fitT)
  => ProgressPrinter            -- ^ Progress printer instance.
  -> Int                        -- ^ Current simulation id.
  -> EvolutionParams geneT fitT -- ^ Evolution parameters.
  -> Population geneT           -- ^ Initial population.
  -> IO (Population geneT)      -- ^ Finish population.
evolutionIO printer simId config initPopulation
  = foldM nextGen initPopulation [1..(nGenerations config)]
  where
    nextGen currentGen i = do
      printProgress printer $ progressBar simId i (nGenerations config)
      return $!! generation (replaceSeed config i) currentGen
    seeds = take (nGenerations config) $ randoms $ mkStdGen (seed config)
    replaceSeed c i = c {seed = seeds !! (i - 1)}
